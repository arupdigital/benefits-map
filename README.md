# Benefits Mapping
This project helps automate creation of benefits maps using the Graphviz library.

## Setup 
In a virtual environment, install requirements with 

`pip install -r requirements.txt`

or on Mac

`pip3 install -r requirements.txt`

## Run 
Run the main script from the command line at the project root using 

`python run.py` 

or 

`python3 run.py`